﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vigenere_Cipher_ASPNET.Interfaces;
using Vigenere_Cipher_ASPNET.Models;

namespace Vigenere_Cipher_ASPNET.Mocks
{
    public class MockCrypto : ICryptoable
    {

        static Dictionary<string, int> alphabet { get; } = new Dictionary<string, int>
        {
            { "а", 0 },
            { "б", 1 },
            { "в", 2 },
            { "г", 3 },
            { "д", 4 },
            { "е", 5 },
            { "ё", 6 },
            { "ж", 7 },
            { "з", 8 },
            { "и", 9 },
            { "й", 10 },
            { "к", 11 },
            { "л", 12 },
            { "м", 13 },
            { "н", 14 },
            { "о", 15 },
            { "п", 16 },
            { "р", 17 },
            { "с", 18 },
            { "т", 19 },
            { "у", 20 },
            { "ф", 21 },
            { "х", 22 },
            { "ц", 23 },
            { "ч", 24 },
            { "ш", 25 },
            { "щ", 26 },
            { "ъ", 27 },
            { "ы", 28 },
            { "ь", 29 },
            { "э", 30 },
            { "ю", 31 },
            { "я", 32 }
        };
        static string alphabetKeys { get; } = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        static int startROT { get; } = 0; // Vigenere square starts with ROT0 ('а' becomes 'а')

        public Vigenere vigenere;

        public string GetDecryptedText  { get { return vigenere.DecryptedText; } }
        public string GetEncryptedText  { get { return vigenere.EncryptedText; } }
        public string GetKey            { get { return vigenere.Key; } }
        public bool   isEncrypting      { get { return vigenere.isEncrypting; } }


        public void Initalize(string text, string key, bool isEncrypting)
        {
            vigenere = new Vigenere();



            vigenere.Key = key.ToLower().Trim(' ');
            KeyParse();

            if (text == null || text == "")
            {
                vigenere.DecryptedText = vigenere.EncryptedText = "Enter some text!";
                return;
            }


            vigenere.DefaultText = text;
            Decrypt();
            Encrypt();

            vigenere.isEncrypting = isEncrypting;
        }

        public void Decrypt()
        {
            int i = 0; // iterator for text
            int rot = 0; // iterator for Key rotation
            while (vigenere.DefaultText.Length > i)
            {
                string currentChar = vigenere.DefaultText[i].ToString().ToLower();

                if (!alphabet.ContainsKey(currentChar))
                {
                    vigenere.DecryptedText += vigenere.DefaultText[i].ToString();
                    i++;
                    continue;
                }


                if (rot >= vigenere.Key.Length)
                    rot = 0;

                int selfRotation = alphabet[currentChar];
                int fullRotation = selfRotation - vigenere.KeyRotation[rot] - startROT;
                fullRotation = fullRotation < 0 ? fullRotation + alphabet.Count : fullRotation;

                char decChar = alphabetKeys[fullRotation];

                decChar = Char.IsUpper(vigenere.DefaultText[i]) ? Char.ToUpper(decChar) : Char.ToLower(decChar);

                vigenere.DecryptedText += decChar.ToString();

                i++;
                rot++;
            }
        }

        public void Encrypt()
        {
            int i = 0; // iterator for text
            int rot = 0; // iterator for Key rotation
            while (vigenere.DefaultText.Length > i)
            {
                string currentChar = vigenere.DefaultText[i].ToString();

                if (!alphabet.Keys.Contains(vigenere.DefaultText[i].ToString()))
                {
                    vigenere.EncryptedText += vigenere.DefaultText[i].ToString();
                    i++;
                    continue;
                }

                if (rot == vigenere.Key.Trim().Length)
                    rot = 0;

                int selfRotation = alphabet[currentChar];
                int fullRotation = selfRotation + vigenere.KeyRotation[rot] + startROT;
                fullRotation = fullRotation >= alphabet.Count ? fullRotation - alphabet.Count : fullRotation;

                char encChar = alphabetKeys[fullRotation];

                encChar = Char.IsUpper(vigenere.DecryptedText[i]) ? Char.ToUpper(encChar) : Char.ToLower(encChar);

                vigenere.EncryptedText += encChar.ToString();

                i++;
                rot++;
            }
        }

        public void KeyParse()
        {
            string newKey = vigenere.Key;
            List<int> indexWhereIsNotLetter = new List<int>();

            for (int i = 0; i < vigenere.Key.Length; i++)
            {
                bool flag = false;
                foreach (var c in alphabet)
                {
                    if (vigenere.Key[i].ToString() == c.Key)
                    {
                        vigenere.KeyRotation.Add(c.Value);
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    indexWhereIsNotLetter.Add(i);
            }

            foreach (var item in indexWhereIsNotLetter)
            {
                newKey = newKey.Substring(0, item) + newKey.Substring(item + 1);
            }

            vigenere.Key = newKey;
        }
    }
}
