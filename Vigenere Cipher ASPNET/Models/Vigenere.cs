﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vigenere_Cipher_ASPNET.Models
{
    public class Vigenere
    {
        public string       DefaultText     { get; set; }
        public string       DecryptedText   { get; set; }
        public string       EncryptedText   { get; set; }
        public string       Key             { get; set; }
        public bool         isEncrypting    { get; set; }
        public List<int>    KeyRotation     { get; set; } = new List<int>();
    }
}
