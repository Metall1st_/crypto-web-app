﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vigenere_Cipher_ASPNET.Interfaces;
using Vigenere_Cipher_ASPNET.Models;
using Vigenere_Cipher_ASPNET.ViewModels;
using System.Security.Cryptography.Pkcs;
using System.IO;

namespace Vigenere_Cipher_ASPNET.Controllers
{
    public class CryptoController : Controller
    {
        private readonly ILogger<CryptoController> _logger;
        private readonly ICryptoable _crypto;

        private static readonly string DefaultKey = "скорпион";

        VigenereViewModel vigenere = new VigenereViewModel();

        public CryptoController(ILogger<CryptoController> logger, ICryptoable crypto)
        {
            _logger = logger;
            _crypto = crypto;
        }

        [HttpGet]
        public IActionResult DownloadFile()
        {
            System.IO.File.WriteAllText(vigenere.path + vigenere.filename, vigenere.isEncrypting ? vigenere.EncryptedText : vigenere.DecryptedText);
            return View("Result");
        }

        [HttpPost]
        public IActionResult Crypto(string isEncrypting, string key, string text)
        {
            bool isEnc = isEncrypting == "enc" ? true : false;

            if (key == null || key == "")
                _crypto.Initalize(text, DefaultKey, isEnc);
            else
                _crypto.Initalize(text, key, isEnc);

            vigenere.DecryptedText = _crypto.GetDecryptedText;
            vigenere.EncryptedText = _crypto.GetEncryptedText;
            vigenere.Key = _crypto.GetKey;
            vigenere.isEncrypting = isEnc;

            return View("Result", model: vigenere);
        }

        public IActionResult Crypto()
        {
            return View();
        }

        public IActionResult Index()
        {
            return View("Crypto");
        }

        public IActionResult Result()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
