﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vigenere_Cipher_ASPNET.ViewModels
{
    public class VigenereViewModel
    {
        public string DefaultText   { get; set; }
        public string EncryptedText { get; set; }
        public string DecryptedText { get; set; }
        public string Key           { get; set; }
        public bool   isEncrypting   { get; set; }


        public readonly string path = @System.IO.Directory.GetCurrentDirectory();
        public readonly string filename = @"\cryptoresult.txt";
    }
}
