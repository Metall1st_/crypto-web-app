# README #

This README documents for what is this App intended.

### What is this repository for? ###
### ����� ���� �����������? ###

#### ENGLISH #####

* Encryption to Vigenere code with a custom *key*
* Decryption from Vigenere code with a custom *key*
* Expression my own gratitude to *Alexander Isaev* and *First Line Software* company for the course

#### ������� ####

* ���������� � ��� ��������, ��������� ���������������� *����*
* ����������� �� ���� ��������, ��������� ���������������� *����*
* ��������� ���� ������������� *���������� ������* � �������� *First Line Software* �� ��������������� ����

### What was done for this application? ###
### ��� ���� ����� ��� ����������? ###

#### ENGLISH #####

* Vigenere algorithm was created with the following features:
	1. Custom *key*
	1. *Key* parsing (Defining *ROT* for every character)
	1. *Encryption*
	1. *Decryption*
	1. Current state (*Enc*/*Dec*)
	1. Opportunity to download result (*kinda stupid, but ok*)
	1. Possibility of getting any *value* (safe)
* Front-end side of the application with pleasant design and some simple *scripts*
* Back-end side with the competent logic (class realises *interface* and *model*)
* Communication between sides realised according to MVC standarts (via *controller*)
* Page navigation is also done simple but beautiful
* Unit tests cover the server side of the app with most of difficult requests

#### ������� #####

* �������� ���������� �������� ���������� �� ���������� �������������:
	1. ���������������� *����*
	1. ������� *�����* (����������� *ROT* ��� ������� �������)
	1. *����������*
	1. *�����������*
	1. �������� ��������� (*����*/*�������*)
	1. ����������� ���������� ����� � ����������� (*����������, � ��������*)
	1. ����������� ��������� ������ *��������* (���������)
* ������� ���������� � �������� �������� � ��������� ����������� *��������*
* ������� ������� � ��������� ������� (����� ��������� *���������* � *������*)
* ������� ����� ��������� ����������� � ������������ �� ���������� MVC (� ������� *�����������*)
* ��������� �� ��������� ����� ������� ������, �� �������
* ����-����� ��������� ��������� ������� ���������� � ������� ����������� ������� ��������
